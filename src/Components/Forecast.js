import React, { Component } from 'react';
import { connect } from 'react-redux'
import moment from 'moment'
import { groupBy, toPairs } from 'lodash'

import './Forecast.css'

class Forecast extends Component {

  render() {
    if (!this.props.forecast) return null


    const forecastDays = toPairs(groupBy(this.props.forecast, ((item) => {
      return moment(item.time * 1000).format("YYYY MM DD")
    })))

    const forecastElements = forecastDays.map((day, index) => {
      const items = day[1].map((item, index2) => {
        return (
          <tr key={index2}>
            <td>{ moment(item.time * 1000 ).format("HH:mm")  }</td>
            <td>{item.overview}</td>
            <td>{item.temp_min}°C</td>
            <td>{item.temp_max}°C</td>
          </tr>
        )
      })
      return (
        <div className="forecastItem"  key={index}>
          <h3>{day[0]}</h3>
          <table className="forecastTable">
           {items}
          </table>
        </div>
      )
    })
  
    return (
      <div className="forecast">
        {forecastElements}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    forecast: state.forecast,
  }
}

export default connect(mapStateToProps)(Forecast)
