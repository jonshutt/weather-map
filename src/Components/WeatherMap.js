import React, { Component } from 'react'
import { connect } from 'react-redux'
import { setMessage, requestForecast } from '../actions';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet'
import  'leaflet/dist/leaflet.css'
import './WeatherMap.css'

class WeatherMap extends Component {

  constructor() {
    super();
    this.state = {
      lat: 57,
      lng: -3,
      zoom: 7,
    };
    this.onClickMap = this.onClickMap.bind(this)
  }

  onClickMap(e)  {
    this.props.setMessage(`Getting forecast for ${e.latlng.lat}, ${e.latlng.lng}`)
    this.props.requestForecast(e.latlng.lat, e.latlng.lng)
  }

  render() {
    const position = [this.state.lat, this.state.lng]

    return (
      <Map className="WeatherMap" center={position} zoom={this.state.zoom} onClick={this.onClickMap}>
        <TileLayer
          attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
      </Map>
    )
  }
}




function mapStateToProps(state) {
  return {
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setMessage: (message) => dispatch(setMessage(message)),
    requestForecast: (lat,lng) => dispatch(requestForecast(lat,lng))
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(WeatherMap)