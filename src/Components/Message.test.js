import React from 'react'
import {shallow} from 'enzyme'
import Message from './Message'
import configureStore from 'redux-mock-store'
const mockStore = configureStore()

test('Display correct message if sent ', () => {
  const message = shallow(<Message store={mockStore({ message: 'This is my message' })} />).dive()
  expect(message.text()).toEqual('This is my message')
  expect(message.hasClass('message')).toBe(true);
})

test('Display nothing if no message', () => {
  const message = shallow(<Message store={mockStore({ message: null })} />).dive()
  expect(message.html()).toEqual(null)
})