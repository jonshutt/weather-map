import React from 'react'
import {shallow} from 'enzyme'
import Forecast from './Forecast'
import { expect } from 'chai';

import configureStore from 'redux-mock-store'
const mockStore = configureStore()

const mockState = {
    forecast: [
        {
            time: 152555,
            overview: 'rain',
            temp_min: 14,
            temp_max: 18,
        },
        {
            time: 152555 + 86400,
            overview: 'sun',
            temp_min: 15,
            temp_max: 28,
        },
        {
            time: 152555 + 86400,
            overview: 'snow',
            temp_min: -20,
            temp_max: -10,
        }
    ]
}

test('Display 2 days', () => {
  const forecast = shallow(<Forecast store={mockStore(mockState)} />).dive()
  expect(forecast.find('.forecastItem')).to.have.length(2)
})

test('Display 3 rows of data', () => {
  const forecast = shallow(<Forecast store={mockStore(mockState)} />).dive()
  expect(forecast.find('tr')).to.have.length(3)
})

test('Display correct details in a row', () => {
  const forecast = shallow(<Forecast store={mockStore(mockState)} />).dive()
  expect(forecast.find('tr').at(2).find('td').at(1).text()).to.equal('snow')
  expect(forecast.find('tr').at(2).find('td').at(2).text()).to.equal('-20°C')
  expect(forecast.find('tr').at(2).find('td').at(3).text()).to.equal('-10°C')
})
