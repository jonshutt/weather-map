import React, { Component } from 'react';
import { connect } from 'react-redux'

import './Message.css'

class Message extends Component {

  render() {
    if (!this.props.message) return null

    return (
      <div className="message">
        {this.props.message}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    message: state.message,
  }
}

export default connect(mapStateToProps)(Message)
