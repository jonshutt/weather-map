import React from 'react'
import {shallow} from 'enzyme'
import WeatherMap from './WeatherMap'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk';
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

test('Display a map ', () => {
  const map = shallow(<WeatherMap store={mockStore({})} />).dive()
  expect(map.hasClass('WeatherMap')).toBe(true);
})

test('Handle click event', () => {
    const store = mockStore({})
    const map = shallow(<WeatherMap store={store} />).dive()
    const requestForecast = jest.fn();
    const setMessage = jest.fn();
    
    map.simulate('click', {latlng: { lat: 57, lng: -3}});

    const actions = store.getActions();
    expect(actions).toEqual([ { type: 'SET_MESSAGE', "message": "Getting forecast for 57, -3"  } ]);
  });
