import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import moxios from 'moxios';
import {expect} from 'chai';
import { requestForecast } from './index';
import * as actionType from './ActionType';
import getPostsMock from '../mocks/forecast';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('getPosts actions', () => {

  beforeEach(function () {
    moxios.install();
  });

  afterEach(function () {
    moxios.uninstall();
  });

  it('calls SET_FORECAST after getting forecast', () => {
    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
        response: getPostsMock,
      });
    });

    const expectedActions = [
      { type: actionType.SET_MESSAGE, message: '' },
      { type: actionType.SET_FORECAST, data: getPostsMock },
    ];

    const store = mockStore({ forecast: {} })

    return store.dispatch(requestForecast(57,-3)).then(() => {
      expect(store.getActions()).to.deep.equal(expectedActions);
    });
  });
});