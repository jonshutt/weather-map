import * as actionType from './ActionType';
import axios from 'axios'


export const setMessage = (message) => ({
  type: actionType.SET_MESSAGE,
  message
});



export const requestForecast = (lat, lng) => {
  return dispatch => {

    const key = '0c45c95a83007c2d56561c66a4db1478';

    return axios.get(`http://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lng}&appid=${key}`)
      .then((response) => {
        dispatch(setMessage(''));
        dispatch(receiveForecast(response.data))
        return response
      })
      .catch((error) => {
        dispatch(setMessage('Failed to load forecast'))
        return error
      });
  }
}


const receiveForecast = (data) => {
  return {
    type: actionType.SET_FORECAST,
    data
  }
}

