import React, { Component } from 'react';
import './App.css';
import WeatherMap from './Components/WeatherMap'
import Message from './Components/Message'
import Forecast from './Components/Forecast'

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="leftCol">
          <Message />
          <Forecast />
        </div>
        <div className="rightCol">
          <WeatherMap />
        </div>
      </div>
    );
  }
}

export default App;
