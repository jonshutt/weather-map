import * as actionType from '../actions/ActionType';

const forecastReducer = (state = [], action) => {
  switch (action.type) {
    case actionType.SET_FORECAST:  
    
      const forecast = action.data.list.map((item) => {
        return {
          time: item.dt,
          overview: item.weather[0].main,
          temp_min: Math.round (-273 + item.main.temp_min),
          temp_max:Math.round (-273 + item.main.temp_max),
        }
      })

      return forecast
    default:
      return state
  }
}

export default forecastReducer;
