import forecastReducer from './forecastReducer'
import getPostsMock from '../mocks/forecast';
import {expect} from 'chai';

it('returns initial state', () => {
  expect(forecastReducer(undefined, {})).to.deep.equal([]);
});

it('returns correct state given data passed', () => {
  const action = {type: 'SET_FORECAST', data: getPostsMock};
  const expected = {
    time: 1525597200,
    overview: 'Clear',
    temp_min: 1,
    temp_max: 2
  }
    
  expect(forecastReducer({}, action)).to.be.an('array');
  expect(forecastReducer({}, action)).to.include.deep.members([expected]);
});