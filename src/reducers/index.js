import { combineReducers } from 'redux';

import forecastReducer from './forecastReducer';
import messageReducer from './messageReducer';

const reducers = combineReducers({
  forecast: forecastReducer,
  message: messageReducer,
})

export default reducers
