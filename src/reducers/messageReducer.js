import * as actionType from '../actions/ActionType';

const messageReducer = (state = 'Click map to load a forecast', action) => {
  switch (action.type) {
    case actionType.SET_MESSAGE:    
      return action.message
    default:
      return state
  }
}

export default messageReducer;
