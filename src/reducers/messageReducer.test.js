import messageReducer from './messageReducer'

it('returns initial state', () => {
  expect(messageReducer(undefined, {})).toEqual('Click map to load a forecast');
});


it('returns set message', () => {
const action = {type: 'SET_MESSAGE', message: 'New Message'};
  expect(messageReducer('My message', action)).toEqual('New Message');
});