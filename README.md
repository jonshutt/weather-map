Basic weather map allowing user to click on a map and view forecast of that location.

## install

yarn

## run

yarn start

## build

yarn build

## test

yarn test